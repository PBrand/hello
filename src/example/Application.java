package example;

public class Application {

    static final String HELLO_WORLD_MESSAGE = "Hello World!";

    public static void main(String[] args) {
        System.out.println(HELLO_WORLD_MESSAGE);
    }
}
